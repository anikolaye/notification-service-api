package com.laundryvroom.notification.service.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.laundryvroom.notification.service.api.domain.EmailTemplate;
import com.laundryvroom.notification.service.api.domain.NotificationEmail;
import com.laundryvroom.notification.service.api.domain.NotificationEmailRecipient;
import com.laundryvroom.notification.service.api.domain.Template;
import com.laundryvroom.notification.service.api.domain.TemplateSearch;
import com.laundryvroom.notification.service.api.model.EmailRecipientDTO;
import com.laundryvroom.notification.service.api.model.NotificationDTO;
import com.laundryvroom.notification.service.api.model.PushDTO;
import com.laundryvroom.notification.service.api.model.RecipientDTO;
import com.laundryvroom.notification.service.api.model.SendNotificationDTO;
import com.laundryvroom.notification.service.api.model.SmsDTO;
import com.laundryvroom.notification.service.api.services.NotificationService;
import com.laundryvroom.notification.service.api.services.TemplateService;
import com.laundryvroom.notification.service.api.type.Channel;
import com.laundryvroom.notification.service.api.type.CopyType;
import com.laundryvroom.notification.service.api.type.TemplateStatus;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by alexnikolayevsky on 11/23/17.
 */
@RunWith(JMockit.class)
public class SendNotificationControllerTest {
    @Tested
    private SendNotificationController controllerUnderTest = new SendNotificationController();

    @Injectable
    protected NotificationService notificationService;

    @Injectable
    private TemplateService templateService;

    @Injectable
    private ObjectMapper mapper;

    private MockMvc mockMvc;
    private final static String VALID_TEMPLATE_NAME = "Template";
    private final static String VALID_RECIPIENT_NAME = "Alex";
    private final static String VALID_RECIPIENT_EMAIL = "test@user.com";


    private List<RecipientDTO> validRecipients;
    private SendNotificationDTO validDTO;
    private SendNotificationDTO noRecipientsDTO;
    private SendNotificationDTO noTemplateNameDTO;
    private SendNotificationDTO noChannelsDTO;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(controllerUnderTest)
                .build();
        validRecipients = new ArrayList<>();
        validRecipients.add(getValidRecipient());

        this.validDTO = getValidDTO();
        this.noRecipientsDTO = new SendNotificationDTO();
        this.noTemplateNameDTO = new SendNotificationDTO();
        this.noTemplateNameDTO.setRecipients(validRecipients);

        this.noChannelsDTO = new SendNotificationDTO();
        this.noChannelsDTO.setRecipients(validRecipients);
        this.noChannelsDTO.setTemplateName("Valid");
    }



    @Test
    public void testSendNotificationWithValidDTOReturns200() throws Exception {
        new Expectations() {{
            templateService.findTemplates((TemplateSearch) any); times = 1; result = getTemplates(1);
            mapper.convertValue(any, NotificationEmail.class); result = new NotificationEmail();
            mapper.convertValue(any, NotificationEmailRecipient[].class); result = new NotificationEmailRecipient[2];
            mapper.writeValueAsString(any); result = "json";
            mapper.convertValue(any, NotificationDTO.class); times = 1; result = new NotificationDTO();
        }};
        performPostRequest(this.validDTO).andExpect(status().isOk());
    }

    @Test
    public void testSendNotificationValidDTOFindsTemplate() throws Exception {
        new Expectations() {{
            templateService.findTemplates((TemplateSearch) any); times = 1;
        }};
        performPostRequest(this.validDTO);
    }

    @Test
    public void testSendNotificationValidDTOReturnsNotificationDTO() throws Exception {
        new Expectations() {{
            templateService.findTemplates((TemplateSearch) any); times = 1; result = getTemplates(1);
            mapper.convertValue(any, NotificationDTO.class); result = new NotificationDTO();
        }};

        MvcResult result = performPostRequest(this.validDTO).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String content = result.getResponse().getContentAsString();
        NotificationDTO actual = mapper.readValue(content, NotificationDTO.class);
        Assert.assertNotNull(actual);
    }

    @Test
    public void testSendNotificationValidDTOFindsTemplateWithStatusEnabledAndDTOName() throws Exception {
        performPostRequest(this.validDTO);
        new Verifications() {{
            TemplateSearch search;
            templateService.findTemplates(search = withCapture()); times = 1;
            Assert.assertEquals(VALID_TEMPLATE_NAME, search.getTemplateName());
            Assert.assertEquals(1, search.getTemplateStatuses().size());
            Assert.assertEquals(TemplateStatus.ENABLED, search.getTemplateStatuses().get(0));
        }};
    }

    @Test
    public void testSendNotificationValidDTOFindsTemplateWithStatusReturnsNullThrows404() throws Exception {
        new Expectations() {{
            templateService.findTemplates((TemplateSearch) any); times = 1; result = null;
        }};
        performPostRequest(this.validDTO).andExpect(status().isNotFound());
    }

    @Test
    public void testSendNotificationWithNoRecipientsReturns400() throws Exception {
        performPostRequest(this.noRecipientsDTO).andExpect(status().isBadRequest());
    }

    @Test
    public void testSendNotificationWithNoTemplateNameReturns400() throws Exception {
        performPostRequest(this.noTemplateNameDTO).andExpect(status().isBadRequest());
    }

    @Test
    public void testSendNotificationWithNoChannelsReturns400() throws Exception {
        performPostRequest(this.noChannelsDTO).andExpect(status().isBadRequest());
    }

    @Test
    public void testSendNotificationValidDTOFailsToMapThrows500() throws Exception {
        new Expectations() {{
            templateService.findTemplates((TemplateSearch) any); times = 1; result = getTemplates(1);
            mapper.convertValue(any, NotificationEmail.class); times = 1; result = new IllegalArgumentException();
        }};
        performPostRequest(this.validDTO).andExpect(status().isInternalServerError());
    }

    private ResultActions performPostRequest(SendNotificationDTO dto) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mockMvc.perform(MockMvcRequestBuilders.post("/notification/send")
                .content(mapper.writeValueAsString(dto))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON));
    }

    private SendNotificationDTO getValidDTO() {
        SendNotificationDTO dto = new SendNotificationDTO();
        dto.setTemplateName(VALID_TEMPLATE_NAME);
        dto.setRecipients(validRecipients);
        Set<Channel> channelSet = new HashSet<>();
        channelSet.add(Channel.EMAIL);
        dto.setChannels(channelSet);
        return dto;
    }

    private RecipientDTO getValidRecipient() {
        RecipientDTO recipient = new RecipientDTO();
        recipient.setEmailRecipient(getEmailRecipient());
        recipient.setPushRecipient(getPushRecipient());
        recipient.setSmsRecipient(getSmsRecipient());
        return recipient;
    }

    private EmailRecipientDTO getEmailRecipient() {
        EmailRecipientDTO dto = new EmailRecipientDTO();
        dto.setCopyType(CopyType.TO);
        dto.setEmail(VALID_RECIPIENT_EMAIL);
        dto.setName(VALID_RECIPIENT_NAME);
        return dto;
    }

    private PushDTO getPushRecipient() {
        return new PushDTO();
    }

    private SmsDTO getSmsRecipient() {
        return new SmsDTO();
    }

    private List<Template> getTemplates(Integer size) {
        Integer i = 0;
        List<Template> templatesToReturn = new ArrayList<>();
        while(i < size) {
            i++;
            Template template = new Template();
            template.setId(i);
            template.setTemplateName(VALID_TEMPLATE_NAME);
            template.setEmailTemplate(new EmailTemplate());
            templatesToReturn.add(template);
        }
        return templatesToReturn;
    }
}