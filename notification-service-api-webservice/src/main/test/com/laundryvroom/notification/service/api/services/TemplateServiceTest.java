package com.laundryvroom.notification.service.api.services;

import com.laundryvroom.notification.service.api.domain.Template;
import com.laundryvroom.notification.service.api.domain.TemplateSearch;
import com.laundryvroom.notification.service.api.respositories.TemplateRepository;
import com.laundryvroom.notification.service.api.services.impl.TemplateServiceImpl;
import com.laundryvroom.notification.service.api.type.TemplateStatus;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by alexnikolayevsky on 11/21/17.
 */
@RunWith(JMockit.class)
public class TemplateServiceTest {
    @Tested
    private TemplateService serviceUnderTest = new TemplateServiceImpl();

    @Injectable
    private TemplateRepository repository;

    final static Integer VALID_TEMPLATE_ID = 1;
    final static String VALID_TEMPLATE_NAME = "Valid Template";
    private Template validTemplate;
    private TemplateSearch validTemplateSearch;

    @Before
    public void setup() throws Exception {
        List<TemplateStatus> statuses = new ArrayList<>();
        statuses.add(TemplateStatus.ENABLED);
        this.validTemplate = new Template();
        this.validTemplate.setId(VALID_TEMPLATE_ID);
        this.validTemplateSearch =  new TemplateSearch.Builder()
                .templateName(VALID_TEMPLATE_NAME)
                .templateStatuses(statuses)
                .build();
    }

    //// Start getById tests ////
    @Test
    public void testGetByIdWithValidIdReturnsTemplate() throws Exception {
        Assert.assertNotNull(serviceUnderTest.getById(VALID_TEMPLATE_ID));
    }

    @Test
    public void testGetByIdWithValidIdInvokesRepository() throws Exception {
        new Expectations() {{
            repository.findOne(VALID_TEMPLATE_ID); times = 1;
        }};

        serviceUnderTest.getById(VALID_TEMPLATE_ID);
    }

    @Test
    public void testGetByIdReturnsRepositoryResult() throws Exception {
        new Expectations() {{
            repository.findOne(VALID_TEMPLATE_ID); times = 1; result = validTemplate;
        }};

        Template actual = serviceUnderTest.getById(VALID_TEMPLATE_ID);
        Assert.assertEquals(this.validTemplate, actual);
    }
    //// End getById tests ////

    //// Start save tests ////
    @Test
    public void testSaveWithValidIdReturnsTemplate() throws Exception {
        Assert.assertNotNull(serviceUnderTest.save(this.validTemplate));
    }

    @Test
    public void testSaveWithValidTemplateInvokesRepository() throws Exception {
        new Expectations() {{
            repository.save(validTemplate); times = 1;
        }};

        serviceUnderTest.save(this.validTemplate);
    }

    @Test
    public void testSaveReturnsRepositoryResult() throws Exception {
        new Expectations() {{
            repository.save(validTemplate); times = 1; result = validTemplate;
        }};

        Template actual = serviceUnderTest.save(this.validTemplate);
        Assert.assertEquals(this.validTemplate, actual);
    }
    //// End save tests ////
    //// Start find tests ////
    @Test
    public void testFindTemplatesReturns() throws Exception {
        Assert.assertNotNull(serviceUnderTest.findTemplates(this.validTemplateSearch));
    }

    @Test
    public void testFindTemplateWithValidTemplateSearchInvokesRepository() throws Exception {
        new Expectations() {{
            repository.findAll(validTemplateSearch); times = 1;
        }};

        serviceUnderTest.findTemplates(this.validTemplateSearch);
    }

    @Test
    public void testFindTemplateReturnsRepositoryResult() throws Exception {
        List<Template> templates = new ArrayList<>();
        templates.add(this.validTemplate);
        new Expectations() {{
            repository.findAll(validTemplateSearch);  times = 1; result = templates;
        }};

        List<Template> actual = serviceUnderTest.findTemplates(this.validTemplateSearch);
        Assert.assertEquals(templates.size(), actual.size());
        Assert.assertEquals(templates.get(0), actual.get(0));

    }
    //// End  find tests ////
}
