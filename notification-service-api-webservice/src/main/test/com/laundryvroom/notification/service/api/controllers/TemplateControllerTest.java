package com.laundryvroom.notification.service.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.laundryvroom.notification.service.api.domain.Template;
import com.laundryvroom.notification.service.api.domain.TemplateSearch;
import com.laundryvroom.notification.service.api.model.TemplateDTO;
import com.laundryvroom.notification.service.api.services.TemplateService;
import com.laundryvroom.notification.service.api.type.TemplateStatus;
import mockit.Expectations;
import mockit.Injectable;
import mockit.Tested;
import mockit.Verifications;
import mockit.integration.junit4.JMockit;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by alexnikolayevsky on 11/21/17.
 */
@RunWith(JMockit.class)
public class TemplateControllerTest {
    @Tested
    private TemplateController controllerUnderTest = new TemplateController();

    @Injectable
    private TemplateService templateService;

    @Injectable
    private ObjectMapper mapper;

    private MockMvc mockMvc;

    private final String VALID_TEMPLATE_ID = "1";
    private final String INVALID_TEMPLATE_ID = "STRING";
    private final String NOT_FOUND_TEMPLATE_ID = "2";

    private final String VALID_TEMPLATE_NAME = "name";
    private final String[] VALID_STATUSES = new String[]{TemplateStatus.ENABLED.name()};

    private TemplateDTO validTemplateDTO;
    private TemplateDTO notFoundTemplateDTO;
    private TemplateDTO validTemplateDTOWithoutId;

    @Before
    public void setup() throws Exception {
        this.mockMvc = MockMvcBuilders.standaloneSetup(controllerUnderTest)
                .build();


        this.validTemplateDTO = createValidTemplate();
        this.notFoundTemplateDTO = createNotFoundTemplate();
        this.validTemplateDTOWithoutId = createValidTemplateWithoutId();
    }

    ///// Start GetTemplate Tests /////
    @Test
    public void testGetWithValidIdReturns200() throws Exception {
        performGetRequest(VALID_TEMPLATE_ID).andExpect(status().isOk());
    }

    @Test
    public void testGetWithValidIdReturnsTemplateDTO() throws Exception {
        MvcResult result = performGetRequest(VALID_TEMPLATE_ID).andReturn();

        ObjectMapper mapper = new ObjectMapper();
        String content = result.getResponse().getContentAsString();
        TemplateDTO actual = mapper.readValue(content, TemplateDTO.class);
        Assert.assertNotNull(actual);
    }

    @Test
    public void testGetWithValidIdInvokesTemplateServiceToSearch() throws Exception {
        new Expectations() {{
            templateService.getById(Integer.parseInt(VALID_TEMPLATE_ID)); times = 1;
        }};

        performGetRequest(VALID_TEMPLATE_ID);
    }

    @Test
    public void testGetWithValidIdMapsTemplateServiceResultToTemplateDTO() throws Exception {
        final Template template = new Template();
        new Expectations() {{
            templateService.getById(Integer.parseInt(VALID_TEMPLATE_ID)); times = 1; result = template;
            mapper.convertValue(template, TemplateDTO.class); times = 1;
        }};

        performGetRequest(VALID_TEMPLATE_ID);
    }

    @Test
    public void testGetWithValidIdReturnsMappedTemplateDTO() throws Exception {
        final Template template = new Template();
        new Expectations() {{
            templateService.getById(Integer.parseInt(VALID_TEMPLATE_ID)); times = 1; result = template;
            mapper.convertValue(template, TemplateDTO.class); times = 1; result = validTemplateDTO;
        }};

        TemplateDTO actual = controllerUnderTest.getTemplate(Integer.parseInt(VALID_TEMPLATE_ID));
        Assert.assertEquals(this.validTemplateDTO, actual);
    }

    @Test
    public void testGetWithNotFoundIdInvokesTemplateServiceFetch() throws Exception {
        new Expectations() {{
            templateService.getById(Integer.parseInt(NOT_FOUND_TEMPLATE_ID)); times = 1;
        }};

        performGetRequest(NOT_FOUND_TEMPLATE_ID);
    }

    @Test
    public void testGetWithNotFoundIdReturns404() throws Exception {
        new Expectations() {{
            templateService.getById(Integer.parseInt(NOT_FOUND_TEMPLATE_ID)); times = 1; result = null;
        }};

        performGetRequest(NOT_FOUND_TEMPLATE_ID).andExpect(status().isNotFound());
    }

    @Test
    public void testGetWhenMapFailsReturn500() throws Exception {
        final Template template = new Template();
        new Expectations() {{
            templateService.getById(Integer.parseInt(VALID_TEMPLATE_ID)); times = 1; result = template;
            mapper.convertValue(template, TemplateDTO.class); times = 1; result = new IllegalArgumentException("Illegal Argument");
        }};
        performGetRequest(VALID_TEMPLATE_ID).andExpect(status().isInternalServerError());
    }

    @Test
    public void testGetWithBadParameterReturns400() throws Exception {
        performGetRequest(INVALID_TEMPLATE_ID).andExpect(status().isBadRequest());
    }
    ///// End GetTemplate Tests /////

    //// Start CreateTemplate Tests /////
    @Test
    public void testPostWithValidDTOReturns200() throws Exception {
        performPostRequest(this.validTemplateDTOWithoutId).andExpect(status().isOk());
    }

    @Test
    public void testPostWithValidDTOReturnsTemplateDTO() throws Exception {
        MvcResult result = performPostRequest(this.validTemplateDTOWithoutId).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String content = result.getResponse().getContentAsString();
        TemplateDTO actual = mapper.readValue(content, TemplateDTO.class);
        Assert.assertNotNull(actual);
    }

    @Test
    public void testPostWithValidDTOMapsDTOToTemplate() throws Exception {
        new Expectations() {{
           mapper.convertValue(validTemplateDTOWithoutId, Template.class); times = 1;
        }};
        controllerUnderTest.createTemplate(this.validTemplateDTOWithoutId);
    }

    @Test
    public void testPostWithValidDTOInvokesTemplateServiceSaveWithMappedDTO() throws Exception {
        final Template template = new Template();
        new Expectations() {{
            mapper.convertValue(validTemplateDTOWithoutId, Template.class); times = 1; result = template;
            templateService.save(template); times = 1;
        }};
        controllerUnderTest.createTemplate(this.validTemplateDTOWithoutId);
    }

    @Test
    public void testPostWithValidDTOMapsTemplateServiceResultToTemplateDTO() throws Exception {
        final Template template = new Template();
        final Template templateWithId = new Template();
        templateWithId.setId(Integer.parseInt(VALID_TEMPLATE_ID));
        new Expectations() {{
            mapper.convertValue(validTemplateDTOWithoutId, Template.class); times = 1; result = template;
            templateService.save(template); times = 1; result = templateWithId;
            mapper.convertValue(templateWithId, TemplateDTO.class);
        }};
        controllerUnderTest.createTemplate(this.validTemplateDTOWithoutId);
    }

    @Test
    public void testPostWithValidDTOReturnsMappedTemplateDTO() throws Exception {
        final Template template = new Template();
        final Template templateWithId = new Template();
        templateWithId.setId(Integer.parseInt(VALID_TEMPLATE_ID));
        new Expectations() {{
            mapper.convertValue(validTemplateDTOWithoutId, Template.class); times = 1; result = template;
            templateService.save(template); times = 1; result = templateWithId;
            mapper.convertValue(templateWithId, TemplateDTO.class); result = validTemplateDTO;
        }};
        TemplateDTO actual = controllerUnderTest.createTemplate(this.validTemplateDTOWithoutId);
        Assert.assertEquals(this.validTemplateDTO, actual);
    }

    @Test
    public void testPostFailsToMapDTOToTemplateThrows500() throws Exception {
       new Expectations() {{
            mapper.convertValue(any, Template.class); times = 1; result = new IllegalArgumentException("Illegal Argument");
        }};
        performPostRequest(this.validTemplateDTOWithoutId).andExpect(status().isInternalServerError());
    }

    @Test
    public void testPostFailsToMapTemplateToDTOThrows500() throws Exception {
        new Expectations() {{
            mapper.convertValue(any, TemplateDTO.class); times = 1; result = new IllegalArgumentException("Illegal Argument");
        }};
        performPostRequest(this.validTemplateDTOWithoutId).andExpect(status().isInternalServerError());
    }
    //// End CreateTemplate Tests //////
    //// Start UpdateTemplate Tests //////
    @Test
    public void testPutWithValidDTOReturns200() throws Exception {
        performPutRequest(VALID_TEMPLATE_ID, this.validTemplateDTO).andExpect(status().isOk());
    }

    @Test
    public void testPutWithValidDTOReturnsTemplateDTO() throws Exception {
        MvcResult result = performPutRequest(VALID_TEMPLATE_ID, this.validTemplateDTO).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String content = result.getResponse().getContentAsString();
        TemplateDTO actual = mapper.readValue(content, TemplateDTO.class);
        Assert.assertNotNull(actual);
    }

    @Test
    public void testPutWithValidDTOFetchesTemplate() throws Exception {
        new Expectations() {{
            templateService.getById(Integer.parseInt(VALID_TEMPLATE_ID)); times = 1;
        }};
        performPutRequest(VALID_TEMPLATE_ID, this.validTemplateDTO);
    }

    @Test
    public void testPutWithValidDTOMapsDTOToTemplate() throws Exception {
        new Expectations() {{
            mapper.convertValue(validTemplateDTO, Template.class); times = 1;
        }};
        controllerUnderTest.updateTemplate(Integer.parseInt(VALID_TEMPLATE_ID), this.validTemplateDTO);
    }

    @Test
    public void testPutWithValidSavesMappedDTO() throws Exception {
        final Template template = new Template();
        new Expectations() {{
            mapper.convertValue(validTemplateDTO, Template.class); times = 1; result = template;
            templateService.save(template); times = 1;
        }};
        controllerUnderTest.updateTemplate(Integer.parseInt(VALID_TEMPLATE_ID), this.validTemplateDTO);
    }

    @Test
    public void testPutWithValidMapsSavedTemplateToDTO() throws Exception {
        final Template template = new Template();
        final Template templateWithId = new Template();
        templateWithId.setId(Integer.parseInt(VALID_TEMPLATE_ID));
        new Expectations() {{
            mapper.convertValue(validTemplateDTO, Template.class); times = 1; result = template;
            templateService.save(template); times = 1; result = templateWithId;
            mapper.convertValue(templateWithId, TemplateDTO.class);
        }};
        controllerUnderTest.updateTemplate(Integer.parseInt(VALID_TEMPLATE_ID), this.validTemplateDTO);
    }

    @Test
    public void testPutWithValidDTOReturnsMappedTemplateDTO() throws Exception {
        final Template template = new Template();
        final Template templateWithId = new Template();
        templateWithId.setId(Integer.parseInt(VALID_TEMPLATE_ID));
        new Expectations() {{
            mapper.convertValue(validTemplateDTO, Template.class); times = 1; result = template;
            templateService.save(template); times = 1; result = templateWithId;
            mapper.convertValue(templateWithId, TemplateDTO.class); result = validTemplateDTO;
        }};
        TemplateDTO actual = controllerUnderTest.updateTemplate(Integer.parseInt(VALID_TEMPLATE_ID), this.validTemplateDTO);
        Assert.assertEquals(this.validTemplateDTO, actual);
    }
    @Test
    public void testPutWithMisMatchedIdsThrows400() throws Exception {
        performPutRequest(NOT_FOUND_TEMPLATE_ID, this.validTemplateDTO).andExpect(status().isBadRequest());
    }

    @Test
    public void testPutWithNotFoundIdThrows404() throws Exception {
        new Expectations() {{
            templateService.getById(Integer.parseInt(NOT_FOUND_TEMPLATE_ID)); times = 1; result = null;
        }};
        performPutRequest(NOT_FOUND_TEMPLATE_ID, this.notFoundTemplateDTO).andExpect(status().isNotFound());
    }

    @Test
    public void testPutFailsToMapDTOToTemplateThrows500() throws Exception {
        new Expectations() {{
            mapper.convertValue(any, Template.class); times = 1; result = new IllegalArgumentException("Illegal Argument");
        }};
        performPutRequest(VALID_TEMPLATE_ID, this.validTemplateDTO).andExpect(status().isInternalServerError());
    }

    @Test
    public void testPutFailsToMapTemplateToDTOThrows500() throws Exception {
        new Expectations() {{
            mapper.convertValue(any, TemplateDTO.class); times = 1; result = new IllegalArgumentException("Illegal Argument");
        }};
        performPutRequest(VALID_TEMPLATE_ID, this.validTemplateDTO).andExpect(status().isInternalServerError());
    }
    //// End UpdateTemplate tests ////
    //// Start find tests ////
    @Test
    public void testFindWithNoParametersReturns200() throws Exception {
        performFindRequestWithNoParams().andExpect(status().isOk());
    }

    @Test
    public void testFindWithValidParametersReturns200() throws Exception {
        performFindRequestWithMultiParams(VALID_TEMPLATE_NAME, VALID_STATUSES).andExpect(status().isOk());
    }

    @Test
    public void testFindWithValidParametersReturnsTemplateDTOArray() throws Exception {
        MvcResult result = performFindRequestWithMultiParams(VALID_TEMPLATE_ID, VALID_STATUSES).andReturn();
        ObjectMapper mapper = new ObjectMapper();
        String content = result.getResponse().getContentAsString();
        TemplateDTO[] actual = mapper.readValue(content, TemplateDTO[].class);
        Assert.assertNotNull(actual);
    }

    @Test
    public void testFindWithValidParametersInvokesTemplateServiceToSearch() throws Exception {
        performFindRequestWithMultiParams(VALID_TEMPLATE_NAME, VALID_STATUSES);
        new Verifications() {{
            TemplateSearch templateSearch;
            templateService.findTemplates(templateSearch = withCapture()); times = 1;
            Assert.assertEquals(VALID_TEMPLATE_NAME, templateSearch.getTemplateName());
            Assert.assertEquals(TemplateStatus.ENABLED, templateSearch.getTemplateStatuses().get(0));
        }};
    }

    @Test
    public void testFindWithValidParametersMapsTemplateSearchResult() throws Exception {
        final List<TemplateSearch> templateSearchResult = new ArrayList<>();
        new Expectations() {{
            templateService.findTemplates((TemplateSearch) any); times = 1; result = templateSearchResult;
            mapper.convertValue(templateSearchResult, TemplateDTO[].class); times = 1;
        }};
        performFindRequestWithMultiParams(VALID_TEMPLATE_NAME, VALID_STATUSES);
    }

    @Test
    public void testFindWithValidParametersReturnsMappedResult() throws Exception {
        final List<TemplateSearch> templateSearchResult = new ArrayList<>();
        List<TemplateStatus> statuses = new ArrayList<>();
        statuses.add(TemplateStatus.ENABLED);
        TemplateDTO[] expected = new TemplateDTO[1];
        new Expectations() {{
            templateService.findTemplates((TemplateSearch) any); times = 1; result = templateSearchResult;
            mapper.convertValue(templateSearchResult, TemplateDTO[].class); times = 1; result = expected;
        }};
        TemplateDTO[] actual = controllerUnderTest.find(VALID_TEMPLATE_NAME, statuses);
        Assert.assertArrayEquals(expected, actual);
    }

    @Test
    public void testFindTemplatesNotFoundReturns404() throws Exception {
        new Expectations() {{
            templateService.findTemplates((TemplateSearch) any); times = 1; result = null;
        }};
        performFindRequestWithMultiParams(VALID_TEMPLATE_NAME, VALID_STATUSES).andExpect(status().isNotFound());
    }

    @Test
    public void testFindMapThrowsIllegalArgumentReturns500() throws Exception {
        final List<TemplateSearch> templateSearchResult = new ArrayList<>();
        List<TemplateStatus> statuses = new ArrayList<>();
        statuses.add(TemplateStatus.ENABLED);
        new Expectations() {{
            templateService.findTemplates((TemplateSearch) any); times = 1; result = templateSearchResult;
            mapper.convertValue(templateSearchResult, TemplateDTO[].class); times = 1; result = new IllegalArgumentException();
        }};
        performFindRequestWithMultiParams(VALID_TEMPLATE_NAME, VALID_STATUSES).andExpect(status().isInternalServerError());

    }
    //// End find tests /////

    private ResultActions performGetRequest(String id) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.get("/template/{id}"
                .replace("{id}", id))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON));
    }

    private ResultActions performPostRequest(TemplateDTO dto) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mockMvc.perform(MockMvcRequestBuilders.post("/template")
                .content(mapper.writeValueAsString(dto))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON));
    }

    private ResultActions performPutRequest(String id, TemplateDTO dto) throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        return mockMvc.perform(MockMvcRequestBuilders.put("/template/{id}"
                .replace("{id}", id))
                .content(mapper.writeValueAsString(dto))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON));
    }

    private ResultActions performFindRequestWithNoParams() throws Exception {
        return mockMvc.perform( MockMvcRequestBuilders.get("/template")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON));
    }

    private ResultActions performFindRequestWithMultiParams(String name, String[] statuses) throws Exception {
        return mockMvc.perform( MockMvcRequestBuilders.get("/template")
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .param("name", name)
                .param("statuses", statuses));
    }

    private TemplateDTO createValidTemplate() {
        TemplateDTO templateDTO = new TemplateDTO();
        templateDTO.setId(Integer.parseInt(VALID_TEMPLATE_ID));
        return templateDTO;
    }

    private TemplateDTO createNotFoundTemplate() {
        TemplateDTO templateDTO = new TemplateDTO();
        templateDTO.setId(Integer.parseInt(NOT_FOUND_TEMPLATE_ID));
        return templateDTO;
    }
    private TemplateDTO createValidTemplateWithoutId() {
        TemplateDTO templateDTO = new TemplateDTO();
        templateDTO.setTemplateName("Valid Template");
        return templateDTO;
    }
}
