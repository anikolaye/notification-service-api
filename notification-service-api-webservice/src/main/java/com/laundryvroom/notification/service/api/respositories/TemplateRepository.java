package com.laundryvroom.notification.service.api.respositories;

import com.laundryvroom.notification.service.api.domain.EmailTemplate;
import com.laundryvroom.notification.service.api.domain.Template;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by alexnikolayevsky on 10/20/17.
 */
public interface TemplateRepository extends JpaRepository<Template, Integer>, JpaSpecificationExecutor<Template> {
}
