package com.laundryvroom.notification.service.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

/**
 * Created by alexnikolayevsky on 11/16/17.
 */
@Entity
@Table(name = "notification_email")
public class NotificationEmail {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "notification_fk")
    private Notification notification;

    @OneToMany(targetEntity = NotificationEmailRecipient.class, mappedBy = "notificationEmail", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<NotificationEmailRecipient> recipients;

    @Column(name = "subject")
    private String subject;

    @Column(name= "from_email")
    private String fromEmail;

    @Column(name = "from_name")
    private String fromName;

    @Column(name = "reply_to")
    private String replyTo;

    @Column(name = "text_content")
    private String textContent;

    @Column(name = "html_content")
    private String htmlContent;

    @Column(name = "merged_text_content")
    private String mergedTextContent;

    @Column(name = "merged_html_content")
    private String mergedHtmlContent;

    @Column(name="email_response")
    private String emailResponse;

    @Column(name="email_response_id")
    private String emailResponseId;

    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "lastmodified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public List<NotificationEmailRecipient> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<NotificationEmailRecipient> recipients) {
        this.recipients = recipients;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public String getMergedTextContent() {
        return mergedTextContent;
    }

    public void setMergedTextContent(String mergedTextContent) {
        this.mergedTextContent = mergedTextContent;
    }

    public String getMergedHtmlContent() {
        return mergedHtmlContent;
    }

    public void setMergedHtmlContent(String mergedHtmlContent) {
        this.mergedHtmlContent = mergedHtmlContent;
    }

    public String getEmailResponse() {
        return emailResponse;
    }

    public void setEmailResponse(String emailResponse) {
        this.emailResponse = emailResponse;
    }

    public String getEmailResponseId() {
        return emailResponseId;
    }

    public void setEmailResponseId(String emailResponseId) {
        this.emailResponseId = emailResponseId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}
