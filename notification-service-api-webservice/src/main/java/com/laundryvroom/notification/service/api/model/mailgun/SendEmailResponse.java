package com.laundryvroom.notification.service.api.model.mailgun;

/**
 * Created by drobinson on 2/5/2017.
 */
public class SendEmailResponse {
    private String id;
    private String message;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
