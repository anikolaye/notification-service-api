package com.laundryvroom.notification.service.api.services.impl;

import com.google.common.collect.Iterables;
import com.laundryvroom.notification.service.api.domain.Notification;
import com.laundryvroom.notification.service.api.respositories.NotificationRepository;
import com.laundryvroom.notification.service.api.services.NotificationService;
import com.laundryvroom.notification.service.api.type.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by alexnikolayevsky on 5/24/16.
 */
@Service
@Transactional
public class NotificationServiceImpl implements NotificationService {
    @Autowired
    protected NotificationRepository repository;

    public Notification[] findAll() {
        return Iterables.toArray(repository.findAll(), Notification.class);
    }

    public Notification findOne(Integer id) {
        return repository.findOne(id);
    }

    public Notification save(Notification notification) {
        return repository.save(notification);
    }

    public void delete(Integer id) {
        repository.delete(id);
    }

    @Override
    public List<Notification> findByStatus(Status status) {
        return repository.findByStatus(status);
    }
}
