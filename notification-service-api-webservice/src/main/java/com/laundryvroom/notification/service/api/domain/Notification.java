package com.laundryvroom.notification.service.api.domain;


import com.laundryvroom.notification.service.api.type.ChannelStatus;
import com.laundryvroom.notification.service.api.type.Status;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

/**
 * Created by alexnikolayevsky on 5/24/16.
 */
@Entity
@Table(name = "notification")
public class Notification {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "template_id")
    private Integer templateId;

    @Column(name = "template_name")
    private String templateName;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "email_channel")
    private Boolean emailChannel;

    @Column(name = "push_channel")
    private Boolean pushChannel;

    @Column(name = "sms_channel")
    private Boolean smsChannel;

    @Column(name = "immediate_processing")
    private Boolean immediateProcessing;

    @Column(name = "raw_request")
    private String rawRequest;

    @Column(name="email_status")
    @Enumerated(EnumType.STRING)
    private ChannelStatus emailStatus;

    @Column(name="sms_status")
    @Enumerated(EnumType.STRING)
    private ChannelStatus smsStatus;

    @Column(name="push_status")
    @Enumerated(EnumType.STRING)
    private ChannelStatus pushStatus;

    @Column(name="sms_response")
    private String smsResponse;

    @Column(name="push_response")
    private String pushResponse;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = NotificationParameter.class, mappedBy = "notification", cascade = CascadeType.ALL)
    private List<NotificationParameter> parameters;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToOne(targetEntity = NotificationEmail.class, mappedBy = "notification", cascade = CascadeType.ALL)
    private NotificationEmail email;

    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "lastmodified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean isEmailChannel() {
        return emailChannel;
    }

    public void setEmailChannel(Boolean emailChannel) {
        this.emailChannel = emailChannel;
    }

    public Boolean isPushChannel() {
        return pushChannel;
    }

    public void setPushChannel(Boolean pushChannel) {
        this.pushChannel = pushChannel;
    }

    public Boolean isSmsChannel() {
        return smsChannel;
    }

    public void setSmsChannel(Boolean smsChannel) {
        this.smsChannel = smsChannel;
    }

    public Boolean isImmediateProcessing() {
        return immediateProcessing;
    }

    public void setImmediateProcessing(Boolean immediateProcessing) {
        this.immediateProcessing = immediateProcessing;
    }

    public String getRawRequest() {
        return rawRequest;
    }

    public void setRawRequest(String rawRequest) {
        this.rawRequest = rawRequest;
    }

    public ChannelStatus getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(ChannelStatus emailStatus) {
        this.emailStatus = emailStatus;
    }

    public ChannelStatus getSmsStatus() {
        return smsStatus;
    }

    public void setSmsStatus(ChannelStatus smsStatus) {
        this.smsStatus = smsStatus;
    }

    public ChannelStatus getPushStatus() {
        return pushStatus;
    }

    public void setPushStatus(ChannelStatus pushStatus) {
        this.pushStatus = pushStatus;
    }

    public String getSmsResponse() {
        return smsResponse;
    }

    public void setSmsResponse(String smsResponse) {
        this.smsResponse = smsResponse;
    }

    public String getPushResponse() {
        return pushResponse;
    }

    public void setPushResponse(String pushResponse) {
        this.pushResponse = pushResponse;
    }

    public NotificationEmail getEmail() {
        return email;
    }

    public void setEmail(NotificationEmail email) {
        this.email = email;
    }

    public List<NotificationParameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<NotificationParameter> parameters) {
        this.parameters = parameters;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}
