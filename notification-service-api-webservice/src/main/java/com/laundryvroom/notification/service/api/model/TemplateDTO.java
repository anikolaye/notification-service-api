package com.laundryvroom.notification.service.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.laundryvroom.notification.service.api.type.TemplateStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alexnikolayevsky on 11/21/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TemplateDTO {
    private Integer id;
    private String templateName;
    private TemplateStatus status;
    private List<TemplateParameterDTO> defaultParameters = new ArrayList<>();
    private EmailTemplateDTO emailTemplate;
    private Date created;
    private Date lastModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public TemplateStatus getStatus() {
        return status;
    }

    public void setStatus(TemplateStatus status) {
        this.status = status;
    }

    public List<TemplateParameterDTO> getDefaultParameters() {
        return defaultParameters;
    }

    public void setDefaultParameters(List<TemplateParameterDTO> defaultParameters) {
        this.defaultParameters = defaultParameters;
    }

    public EmailTemplateDTO getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(EmailTemplateDTO emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}
