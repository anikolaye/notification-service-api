package com.laundryvroom.notification.service.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.laundryvroom.notification.service.api.type.Channel;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by alexnikolayevsky on 5/24/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SendNotificationDTO {
    private String templateName;
    private Set<Channel> channels;
    private Map<String, String> parameters;
    private List<RecipientDTO> recipients;
    private UserPreferencesDTO userPreferences;

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Set<Channel> getChannels() {
        return channels;
    }

    public void setChannels(Set<Channel> channels) {
        this.channels = channels;
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(Map<String, String> parameters) {
        this.parameters = parameters;
    }

    public List<RecipientDTO> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<RecipientDTO> recipients) {
        this.recipients = recipients;
    }

    public UserPreferencesDTO getUserPreferences() {
        return userPreferences;
    }

    public void setUserPreferences(UserPreferencesDTO userPreferences) {
        this.userPreferences = userPreferences;
    }

}
