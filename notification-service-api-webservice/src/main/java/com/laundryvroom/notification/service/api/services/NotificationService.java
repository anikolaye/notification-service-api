package com.laundryvroom.notification.service.api.services;

import com.laundryvroom.notification.service.api.domain.Notification;
import com.laundryvroom.notification.service.api.type.Status;

import java.util.List;

/**
 * Created by alexnikolayevsky on 5/24/16.
 */
public interface NotificationService {
    Notification[] findAll();
    Notification findOne(Integer id);
    Notification save(Notification notification);
    void delete(Integer id);

    List<Notification> findByStatus(Status status);
}
