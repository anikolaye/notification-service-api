package com.laundryvroom.notification.service.api.quartz.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.quartz.CronTrigger;
import org.quartz.Trigger;

import java.util.Date;

/**
 * Created by alexnikolayevsky on 9/5/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class TriggerDTO {
    private String name;
    private String group;
    private String cronExpression;
    private Date previousFireTime;
    private Date nextFireTime;

    public TriggerDTO(){}

    public TriggerDTO(Trigger trigger) {
        this.name = trigger.getKey().getName();
        this.group = trigger.getKey().getGroup();
        this.previousFireTime = trigger.getPreviousFireTime();
        this.nextFireTime = trigger.getNextFireTime();
        if (trigger instanceof CronTrigger) {
            CronTrigger cronTrigger = (CronTrigger) trigger;
            this.cronExpression = cronTrigger.getCronExpression();
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getCronExpression() {
        return cronExpression;
    }

    public void setCronExpression(String cronExpression) {
        this.cronExpression = cronExpression;
    }

    public Date getPreviousFireTime() {
        return previousFireTime;
    }

    public void setPreviousFireTime(Date previousFireTime) {
        this.previousFireTime = previousFireTime;
    }

    public Date getNextFireTime() {
        return nextFireTime;
    }

    public void setNextFireTime(Date nextFireTime) {
        this.nextFireTime = nextFireTime;
    }

}
