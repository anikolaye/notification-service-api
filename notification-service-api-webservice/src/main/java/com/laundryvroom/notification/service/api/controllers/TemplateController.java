package com.laundryvroom.notification.service.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.laundryvroom.notification.service.api.domain.Template;
import com.laundryvroom.notification.service.api.domain.TemplateSearch;
import com.laundryvroom.notification.service.api.exceptions.BadRequestException;
import com.laundryvroom.notification.service.api.exceptions.EntityNotFoundException;
import com.laundryvroom.notification.service.api.exceptions.InternalServerException;
import com.laundryvroom.notification.service.api.model.TemplateDTO;
import com.laundryvroom.notification.service.api.services.TemplateService;
import com.laundryvroom.notification.service.api.type.TemplateStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by alexnikolayevsky on 11/21/17.
 */
@RestController
@RequestMapping(value = "/template")
@Api(value = "Template Service", description = "Notification Template REST Service")
public class TemplateController {
    private static final Logger logger = LoggerFactory.getLogger(TemplateController.class);

    @Autowired
    private TemplateService templateService;

    @Autowired
    private ObjectMapper mapper;

    @ApiOperation(value = "Get Template", httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = TemplateDTO.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public TemplateDTO getTemplate(@PathVariable Integer id) {
        logger.info("Fetching template: {\"id\":{} }", id);
        Template template = templateService.getById(id);
        if (template == null) {
            logger.warn("Template not found: {\"id\":{} }", id);
            throw new EntityNotFoundException("Failed to find template with id " + id);
        }
        try {
            return mapper.convertValue(template, TemplateDTO.class);
        } catch (IllegalArgumentException e) {
            logger.error("Illegal Argument Exception occurred after mapping: {\"id\":{} }", id, e);
            throw new InternalServerException("Illegal Argument Exception occurred after mapping template with id " + id, e);
        }
    }

    @ApiOperation(value = "Create Template", httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = TemplateDTO.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")
    })
    @RequestMapping(method = RequestMethod.POST, produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public TemplateDTO createTemplate(@RequestBody TemplateDTO dto) {
        try {
            logger.info("Create Template requested.");
            Template template = mapper.convertValue(dto, Template.class);
            template = templateService.save(template);
            logger.info("Saved Template { \"id\":{} }", template.getId());
            return mapper.convertValue(template, TemplateDTO.class);
        } catch (IllegalArgumentException e) {
            logger.error("Illegal Argument Exception occurred after mapping", e);
            throw new InternalServerException("Failed to map", e);
        }
    }

    @ApiOperation(value = "Update Template", httpMethod = "PUT")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = TemplateDTO.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")
    })
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public TemplateDTO updateTemplate(@PathVariable Integer id, @RequestBody TemplateDTO dto) {
        logger.info("Update Template requested { \"id\":{} }", id);
        if (!id.equals(dto.getId())) {
            logger.warn("Ids do not match");
            throw new BadRequestException("Ids do not match.");
        }
        Template template = templateService.getById(id);
        if (template == null) {
            logger.warn("Template not found: { \"id\":{} }", id);
            throw new EntityNotFoundException("Template not found for id " + id);
        }
        try {
            template = templateService.save(mapper.convertValue(dto, Template.class));
            logger.info("Update Template succeeded { \"id\":{} }", template.getId());
            return mapper.convertValue(template, TemplateDTO.class);
        } catch (IllegalArgumentException e) {
            logger.error("Illegal Argument Exception occurred after mapping: {\"id\":{} }", id, e);
            throw new InternalServerException("Failed to map", e);
        }
    }

    @ApiOperation(value = "find", nickname = "findTemplates")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success", response = TemplateDTO.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")})
    @RequestMapping(method = RequestMethod.GET, produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public TemplateDTO[] find(@RequestParam(value = "name", required = false) String name,
                              @RequestParam(value = "statuses", required = false) List<TemplateStatus> statuses) {
        logger.info("Finding templates for name {} and statuses {}", name, statuses);

        TemplateSearch search = new TemplateSearch.Builder()
                .templateName(name)
                .templateStatuses(statuses)
                .build();

        List<Template> result = templateService.findTemplates(search);
        if(result == null) {
            logger.info("Template(s) not found with parameters: { \"name\":{}, \"statuses\":{} }", name, statuses.toString());
            throw new EntityNotFoundException("Template(s) not found with parameters name " + name + " statuses " + statuses.toString());
        }
        try {
            return mapper.convertValue(result, TemplateDTO[].class);
        } catch (IllegalArgumentException e) {
            logger.error("Illegal Argument Exception occurred with parameters: { \"name\":{}, \"statuses\":{} }", name, statuses.toString(), e);
            throw new InternalServerException("Failed to map", e);
        }
    }
}