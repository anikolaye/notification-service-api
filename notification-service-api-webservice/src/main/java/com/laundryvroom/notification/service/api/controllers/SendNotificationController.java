package com.laundryvroom.notification.service.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.laundryvroom.notification.service.api.domain.Notification;
import com.laundryvroom.notification.service.api.domain.NotificationEmail;
import com.laundryvroom.notification.service.api.domain.NotificationEmailRecipient;
import com.laundryvroom.notification.service.api.domain.Template;
import com.laundryvroom.notification.service.api.domain.TemplateSearch;
import com.laundryvroom.notification.service.api.exceptions.BadRequestException;
import com.laundryvroom.notification.service.api.exceptions.EntityNotFoundException;
import com.laundryvroom.notification.service.api.exceptions.InternalServerException;
import com.laundryvroom.notification.service.api.model.ApiErrorResponse;
import com.laundryvroom.notification.service.api.model.NotificationDTO;
import com.laundryvroom.notification.service.api.model.RecipientDTO;
import com.laundryvroom.notification.service.api.model.SendNotificationDTO;
import com.laundryvroom.notification.service.api.services.NotificationService;
import com.laundryvroom.notification.service.api.services.TemplateService;
import com.laundryvroom.notification.service.api.type.Channel;
import com.laundryvroom.notification.service.api.type.Status;
import com.laundryvroom.notification.service.api.type.TemplateStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created by alexnikolayevsky on 1/21/17.
 */
@RestController
@RequestMapping(value = "/notification")
@Api(value = "Configuration Service", description = "Notification Configuration REST Service")
public class SendNotificationController {

    static final Logger logger = LoggerFactory.getLogger(SendNotificationController.class);

    @Autowired
    protected NotificationService notificationService;

    @Autowired
    protected TemplateService templateService;

    @Autowired
    private ObjectMapper objectMapper;


    @ApiOperation(value = "Create Notification", response = SendNotificationDTO.class, httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 400, message = "Bad Request", response = ApiErrorResponse.class),
            @ApiResponse(code = 500, message = "Internal Server Error", response = ApiErrorResponse.class),
            @ApiResponse(code = 503, message = "Service Unavailable", response = ApiErrorResponse.class)
    })
    @RequestMapping(value="/send", method = RequestMethod.POST, produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public NotificationDTO saveNotification(@RequestBody SendNotificationDTO dto) {
        logger.info("Save Notification requested: {}", dto.toString());
        if(CollectionUtils.isEmpty(dto.getRecipients())) {
            throw new BadRequestException("Recipients Must Be Set");
        }
        if(dto.getTemplateName() == null) {
            throw new BadRequestException("Template Name Cannot Be Null");
        }

        if(CollectionUtils.isEmpty(dto.getChannels()) ) {
            throw new BadRequestException("At least one channel must be set.");
        }
//        //Fetch enabled template
        Template template = getActiveTemplate(dto.getTemplateName());
        if(template == null) {
            throw new EntityNotFoundException("No enabled templates found for name " + dto.getTemplateName());
        }
        try {
            Notification notification = mapDtoToNotification(dto, template);
            notificationService.save(notification);
            logger.info("Saved notification request: {}", notification.getId());
            return objectMapper.convertValue(notification, NotificationDTO.class);
        }
        catch (Exception e) {
            logger.error("Failed to save Notification", e);
            throw new InternalServerException("Failed to save notification");
        }
    }

    private Template getActiveTemplate(String templateName) {
        List<TemplateStatus> statuses = new ArrayList<>();
        statuses.add(TemplateStatus.ENABLED);
        TemplateSearch search = new TemplateSearch.Builder()
                .templateName(templateName)
                .templateStatuses(statuses)
                .build();
        List<Template> templates = templateService.findTemplates(search);
        if(CollectionUtils.isEmpty(templates)) {
            return null;
        }
        logger.info("Found templates with size {}, returning first in list id {}", templates.size(), templates.get(0).getId());
        return templates.get(0);
    }


    private Notification mapDtoToNotification(SendNotificationDTO dto, Template template) throws Exception {
        Notification notification = new Notification();
        notification.setTemplateId(template.getId());
        notification.setTemplateName(template.getTemplateName());
        // scheduling defaults to IMMEDIATE
        notification.setImmediateProcessing(true);
        notification.setStatus(Status.READY);

        for (Channel c : dto.getChannels()) {
            switch (c) {
                case EMAIL:
                    notification.setEmailChannel(true);
                    setEmailTemplate(template, notification, dto.getRecipients(), dto.getParameters());
                    break;
                case SMS:
                case PUSH:
                default:
                    logger.info("Nothing yet....");
                    break;
            }
        }

        String jsonString = objectMapper.writeValueAsString(dto);
        notification.setRawRequest(jsonString);
        return notification;
    }

    private void setEmailTemplate(Template template, Notification notification, List<RecipientDTO> recipients, Map<String, String> parameters) {
        NotificationEmail notificationEmail = objectMapper.convertValue(template.getEmailTemplate(), NotificationEmail.class);
        notificationEmail.setId(null);
        notificationEmail.setNotification(notification);
        List<NotificationEmailRecipient> convertedRecipients = new ArrayList<>();
        for(RecipientDTO recipientDTO : recipients) {
            NotificationEmailRecipient emailRecipient = objectMapper.convertValue(recipientDTO.getEmailRecipient(), NotificationEmailRecipient.class);
            emailRecipient.setId(null);
            emailRecipient.setNotificationEmail(notificationEmail);
            convertedRecipients.add(emailRecipient);
        }
        notificationEmail.setRecipients(convertedRecipients);
        notification.setEmail(notificationEmail);
    }

}
