package com.laundryvroom.notification.service.api.services.impl;

import com.laundryvroom.notification.service.api.domain.Notification;
import com.laundryvroom.notification.service.api.domain.NotificationEmail;
import com.laundryvroom.notification.service.api.domain.NotificationEmailRecipient;
import com.laundryvroom.notification.service.api.model.mailgun.SendEmailResponse;
import com.laundryvroom.notification.service.api.services.EmailService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.tomcat.util.buf.StringUtils;
import org.glassfish.jersey.client.authentication.HttpAuthenticationFeature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by drobinson on 2/5/2017.
 */
@Service
@Transactional
public class EmailServiceImpl implements EmailService {

    static final Logger logger = LoggerFactory.getLogger(EmailServiceImpl.class);

    static final String SEND_EMAIL_PATH = "/messages";

    @Value("${email.mailgun.baseUrl}")
    private String mailgunBaseUrl;

    @Value("${email.mailgun.apiKey}")
    private String mailgunApiKey;

    @Value("${email.mailgun.host}")
    private String mailgunHost;

    /**
     * Send the email using the mailgun api
     * @return The transaction id
     */
    public SendEmailResponse sendEmail(NotificationEmail email) throws Exception {

        Client client = ClientBuilder.newClient().register(configureBasicAuth());
        String url = mailgunBaseUrl + "/" + mailgunHost;
        WebTarget target = client.target(url).path(SEND_EMAIL_PATH);
        List<String> toRecipients = new ArrayList<>();
        List<String> ccRecipients = new ArrayList<>();
        List<String> bccRecipients = new ArrayList<>();
        for(NotificationEmailRecipient recipient : email.getRecipients()) {
            switch(recipient.getCopyType()) {
                case TO:
                    toRecipients.add(recipient.getEmail());
                    break;
                case CC:
                    ccRecipients.add(recipient.getEmail());
                    break;
                case BCC:
                    bccRecipients.add(recipient.getEmail());
                    break;
                default:
                    logger.info("Copy Type Unsupported");
                    break;
            }
        }
        Form form = new Form();
        form.param("from", email.getFromEmail());
        if(CollectionUtils.isNotEmpty(toRecipients)) {
            form.param("to", StringUtils.join(toRecipients, ','));
        }
        if(CollectionUtils.isNotEmpty(ccRecipients)) {
            form.param("cc", StringUtils.join(ccRecipients, ','));
        }
        if(CollectionUtils.isNotEmpty(bccRecipients)) {
            form.param("bcc", StringUtils.join(bccRecipients, ','));
        }
        form.param("subject", email.getSubject());
        form.param("text", email.getMergedTextContent());
        form.param("html", email.getMergedHtmlContent());
        logger.info("Sending email message to url: [{}]; path: [()]; with params [{}]", new Object[] {url, SEND_EMAIL_PATH, form});

        SendEmailResponse response = target.request(MediaType.APPLICATION_JSON_TYPE)
                        .post(Entity.entity(form,MediaType.APPLICATION_FORM_URLENCODED_TYPE),
                        SendEmailResponse.class);

        logger.info("Received response from send email operation: id={}; message={}", new Object[] {response.getId(), response.getMessage()});

        return response;
    }

    /**
     * Configure the basic auth feature for the jersey client
     * @return A feature for basic auth
     */
    private HttpAuthenticationFeature configureBasicAuth() {
        HttpAuthenticationFeature feature = HttpAuthenticationFeature.basic("api", mailgunApiKey);
        return feature;
    }
}
