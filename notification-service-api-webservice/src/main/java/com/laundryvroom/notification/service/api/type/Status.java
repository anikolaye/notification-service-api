package com.laundryvroom.notification.service.api.type;

/**
 * Created by alexnikolayevsky on 1/21/17.
 */
public enum Status {
    READY,
    SCHEDULED,
    ERROR,
    PARTIALLY_SENT,
    SENT
}
