package com.laundryvroom.notification.service.api.services;

import com.laundryvroom.notification.service.api.domain.Template;
import com.laundryvroom.notification.service.api.domain.TemplateSearch;

import java.util.List;

/**
 * Created by alexnikolayevsky on 11/21/17.
 */
public interface TemplateService {
    Template getById(Integer id);
    Template save(Template template);
    List<Template> findTemplates(TemplateSearch templateSearch);
}
