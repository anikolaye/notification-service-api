package com.laundryvroom.notification.service.api.domain;

import com.laundryvroom.notification.service.api.type.TemplateStatus;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by alexnikolayevsky on 11/8/17.
 */
public class TemplateSearch implements Specification<Template> {

    private String templateName;
    private List<TemplateStatus> templateStatuses;

    public TemplateSearch() {
    }

    public TemplateSearch(Builder builder) {
        templateName = builder.templateName;
        templateStatuses = builder.templateStatuses;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public List<TemplateStatus> getTemplateStatuses() {
        return templateStatuses;
    }

    public void setTemplateStatuses(List<TemplateStatus> templateStatuses) {
        this.templateStatuses = templateStatuses;
    }

    public Predicate toPredicate(Root<Template> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        final Collection<Predicate> predicates = new ArrayList<>();

        if (templateName != null || templateStatuses != null) {
            if (templateName != null) {
                predicates.add(criteriaBuilder.equal(root.get("templateName"), templateName));
            }

            if (templateStatuses != null) {
                Expression<TemplateStatus> exp = root.get("status");
                Predicate predicate = exp.in(templateStatuses);
                predicates.add(predicate);
            }
        }

        return criteriaBuilder.and(predicates.toArray(new Predicate[predicates.size()]));
    }

    public static final class Builder {
        private String templateName;
        private List<TemplateStatus> templateStatuses;

        public Builder() {
        }

        public Builder templateName(String templateName) {
            this.templateName = templateName;
            return this;
        }

        public Builder templateStatuses(List<TemplateStatus> templateStatuses) {
            this.templateStatuses = templateStatuses;
            return this;
        }

        public TemplateSearch build() {
            return new TemplateSearch(this);
        }
    }
}
