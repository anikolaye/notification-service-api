package com.laundryvroom.notification.service.api.util;

import org.apache.commons.beanutils.PropertyUtils;

import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

/**
 * Created by alexnikolayevsky on 9/5/17.
 */
public class DotConfigurationProperties extends Properties {

    @Override
    public String getProperty(String key) {
        if (key == null) {
            return null;
        }
        if (containsKey(key)) {
            return super.getProperty(key);
        }
        try {
            return String.valueOf(PropertyUtils.getProperty(this, key));
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public Set<String> stringPropertyNames() {
        return getFlatPropertyNames(null, this);
    }

    public Properties getFlatProperties() {
        Properties properties = new Properties();
        for (String key : this.stringPropertyNames()) {
            properties.setProperty(key, getProperty(key));
        }
        return properties;
    }

    private Set<String> getFlatPropertyNames(String prefix, Map property) {
        Set<String> propertyNames = new HashSet<String>();
        for (Object o : property.keySet()) {
            String key = (String) o;
            Object value = property.get(key);
            if (prefix != null) {
                key = prefix + "." + key;
            }
            if (value instanceof String) {
                propertyNames.add(key);
            } else if (value instanceof Map){
                propertyNames.addAll(getFlatPropertyNames(key, (Map)value));
            }
        }

        return propertyNames;
    }
}
