package com.laundryvroom.notification.service.api.services.impl;

import com.laundryvroom.notification.service.api.model.SendNotificationDTO;
import com.laundryvroom.notification.service.api.services.EmailService;
import com.laundryvroom.notification.service.api.services.SendNotificationService;
import com.laundryvroom.notification.service.api.type.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Set;

/**
 * Created by alexnikolayevsky on 1/21/17.
 */
public class SendNotificationServiceImpl implements SendNotificationService {

    static final Logger logger = LoggerFactory.getLogger(SendNotificationServiceImpl.class);

    @Autowired
    private EmailService emailService;


    public void sendNotification(SendNotificationDTO notification) {
        if (notification != null) {
            Set<Channel> channels = notification.getChannels();
            if (channels != null) {
                for (Channel c : channels) {
                    switch(c) {
                        case EMAIL:
                            sendEmail(notification);
                            break;
                        case SMS:
                        case PUSH:
                        default:
                            logger.warn("Skipping notification for unsupported channel: [{}]", c.name());
                            break;
                    }
                }
            } else {
//                logger.warn("No channels specified for notification with id [{}]", notification.getId());
            }
        } else {
            logger.warn("Unable to send any notifications for null notification entity.");
        }

    }


    public void sendEmail(SendNotificationDTO notification) {
        if (notification != null) {
//            logger.info("Received email notification request with id [{}]", notification.getId());
            if (notification.getUserPreferences() != null && notification.getUserPreferences().isAllowEmail()) {
//                SendEmailResponse response = emailService.sendEmail(notification.getRecipient().getEmail(), notification.getContent());

//                logger.info("Received transaction id: [{}] for message: [{}]", new Object[]{transactionId, notification});
            }
        } else {
            logger.warn("Unable to send email for null notification entity.");
        }
    }
}
