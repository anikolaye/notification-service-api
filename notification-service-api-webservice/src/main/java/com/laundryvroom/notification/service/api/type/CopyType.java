package com.laundryvroom.notification.service.api.type;

/**
 * Created by alexnikolayevsky.
 */
public enum CopyType {
    TO("to"),
    CC("cc"),
    BCC("bcc");

    private String copyType;

    CopyType(String copyType) {
        this.copyType = copyType;
    }

    public String getCopyType() {
        return this.copyType;
    }
}
