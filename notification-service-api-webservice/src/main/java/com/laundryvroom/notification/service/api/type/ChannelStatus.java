package com.laundryvroom.notification.service.api.type;

/**
 * Created by alexnikolayevsky
 */
public enum ChannelStatus {
    SENT,
    ERROR
}
