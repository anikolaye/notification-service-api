package com.laundryvroom.notification.service.api.quartz.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.quartz.JobDataMap;
import org.quartz.JobKey;

/**
 * Created by alexnikolayevsky on 9/5/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobDetailDTO {
    private String name;
    private String group;
    private String jobClass;
    private JobDataMap jobDataMap = new JobDataMap();

    public JobDetailDTO(){}

    public JobDetailDTO(JobKey jobKey) {
        this.name = jobKey.getName();
        this.group = jobKey.getGroup();
        this.jobClass = jobKey.getClass().getCanonicalName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getJobClass() {
        return jobClass;
    }

    public void setJobClass(String jobClass) {
        this.jobClass = jobClass;
    }

    public JobDataMap getJobDataMap() {
        return jobDataMap;
    }

    public void setJobDataMap(JobDataMap jobDataMap) {
        this.jobDataMap = jobDataMap;
    }
}
