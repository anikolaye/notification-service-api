package com.laundryvroom.notification.service.api.services;

import com.laundryvroom.notification.service.api.domain.NotificationEmail;
import com.laundryvroom.notification.service.api.model.mailgun.SendEmailResponse;

/**
 * Created by drobinson on 2/5/2017.
 */
public interface EmailService {
    SendEmailResponse sendEmail(NotificationEmail email) throws Exception;
}
