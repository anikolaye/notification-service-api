package com.laundryvroom.notification.service.api.type;

/**
 * Created by alexnikolayevsky on 1/21/17.
 */
public enum Channel {
    EMAIL,
    PUSH,
    SMS
}
