package com.laundryvroom.notification.service.api.quartz.job;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.laundryvroom.notification.service.api.domain.Notification;
import com.laundryvroom.notification.service.api.domain.NotificationEmail;
import com.laundryvroom.notification.service.api.model.SendNotificationDTO;
import com.laundryvroom.notification.service.api.model.mailgun.SendEmailResponse;
import com.laundryvroom.notification.service.api.services.EmailService;
import com.laundryvroom.notification.service.api.services.NotificationService;
import com.laundryvroom.notification.service.api.type.ChannelStatus;
import com.laundryvroom.notification.service.api.type.Status;
import org.apache.commons.collections.MapUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//    {
//        "jobDetail": {
//            "name": "NotifyJob",
//            "group": "DEFAULT",
//            "jobClass": "org.quartz.JobKey"
//        },
//        "triggers": [{
//            "name":"NotifyJob",
//            "group":"DEFAULT",
//            "cronExpression":"0 0/10 * 1/1 * ? *"
//        }]
//    }
/**
 * Created by alexnikolayevsky on 10/14/17.
 */
@DisallowConcurrentExecution
public class NotifyJob implements Job {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private NotificationService notificationService;

    @Autowired
    private EmailService emailService;
    

    @Autowired
    private ObjectMapper objectMapper;

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        List<Notification> notifications =  notificationService.findByStatus(Status.READY);

        if (notifications != null && !notifications.isEmpty()) {
            logger.info("Found {} number of notifications ready for sending", notifications.size());
            Integer notificationCount = 0;
            for (Notification notification : notifications) {
                List<Boolean> channelStatuses = new ArrayList<>();
                try {
                    sendEmail(notification, channelStatuses);
                    sendSms(notification, channelStatuses);
                    sendPush(notification, channelStatuses);
                } catch (Exception e) {
                    logger.error("An error occurred sending notifications {\"id\": {} } ", notification.getId(), e);
                    notification.setStatus(Status.ERROR);
                }
                notification.setStatus(determineNotificationStatus(channelStatuses));
                notificationCount++;
                try {
                    logger.info("Updating notification and status. {\"id\":{}, \"status\":{} }", notification.getId(), notification.getStatus());
                    notificationService.save(notification);
                }
                catch (Exception e) {
                    logger.error("An error occurred updating notification request {\"id\": {} }" + notification.getId(), e);
                }
            }
            String msg = "Processed " + notificationCount + " notifications";
            logger.info(msg);
            jobExecutionContext.setResult(msg);
        } else {
            logger.info("No notifications to process");
        }
    }

    private void sendEmail(Notification notification, List<Boolean> channelStatuses) throws Exception {
        if(notification.isEmailChannel() != null && notification.isEmailChannel()) {
            logger.info("Notification being sent through email channel {\"id\": {} }", notification.getId());
            NotificationEmail email = notification.getEmail();
            mergeEmail(email, notification.getRawRequest());
            SendEmailResponse emailResponse = emailService.sendEmail(email);
            channelStatuses.add(emailResponse != null);
            notification.setEmailStatus(ChannelStatus.SENT);
            email.setEmailResponseId(emailResponse.getId());
            email.setEmailResponse(emailResponse.getMessage());
            notification.setEmail(email);
        } else {
            logger.info("Email channel not set for notification {\"id\":{} }", notification.getId());
        }
    }

    private void mergeEmail(NotificationEmail email, String rawRequest) throws Exception {
        SendNotificationDTO notificationDTO = objectMapper.readValue(rawRequest, SendNotificationDTO.class);
        if(notificationDTO != null) {
            String mergedHtml = email.getHtmlContent();
            String mergedText = email.getTextContent();
            if(MapUtils.isNotEmpty(notificationDTO.getParameters())) {
                //loop a Map
                for (Map.Entry<String, String> entry : notificationDTO.getParameters().entrySet()) {
                    mergedHtml = mergedHtml.replaceAll(entry.getKey(), entry.getValue());
                    mergedText = mergedText.replaceAll(entry.getKey(), entry.getValue());
                }
            }
            email.setMergedHtmlContent(mergedHtml);
            email.setMergedTextContent(mergedText);
        }
    }

    private void sendSms(Notification notification,List<Boolean> channelStatuses) {
        if(notification.isSmsChannel() != null && notification.isSmsChannel()) {
            logger.warn("SMS is unsupported");
        }
    }

    private void sendPush(Notification notification, List<Boolean> channelStatuses) {
        if(notification.isPushChannel() != null && notification.isSmsChannel()) {
            logger.warn("Push is unsupported");
        }
    }

    private Status determineNotificationStatus(List<Boolean> channelStatuses) {
        Integer numberOfSuccessStatuses = 0;
        for (Boolean successStatus : channelStatuses) {
            if (successStatus) {
                numberOfSuccessStatuses++;
            }
        }
        if (numberOfSuccessStatuses == 0) {
            return Status.ERROR;
        }
        else if (numberOfSuccessStatuses == channelStatuses.size()) {
            return Status.SENT;
        }
        else { // 0 < numberOfSuccessStatuses < channelStatuses.size()
            return Status.PARTIALLY_SENT;
        }
    }
}
