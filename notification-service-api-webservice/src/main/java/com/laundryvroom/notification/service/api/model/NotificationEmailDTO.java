package com.laundryvroom.notification.service.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Date;
import java.util.List;

/**
 * Created by alexnikolayevsky on 11/23/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationEmailDTO {
    private Integer id;
    private List<NotificationEmailRecipientDTO> recipients;
    private String subject;
    private String fromEmail;
    private String fromName;
    private String replyTo;
    private String textContent;
    private String htmlContent;
    private String emailResponse;
    private String emailResponseId;
    private Date created;
    private Date lastModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<NotificationEmailRecipientDTO> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<NotificationEmailRecipientDTO> recipients) {
        this.recipients = recipients;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public String getEmailResponse() {
        return emailResponse;
    }

    public void setEmailResponse(String emailResponse) {
        this.emailResponse = emailResponse;
    }

    public String getEmailResponseId() {
        return emailResponseId;
    }

    public void setEmailResponseId(String emailResponseId) {
        this.emailResponseId = emailResponseId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}
