package com.laundryvroom.notification.service.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by drobinson on 2/5/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserPreferencesDTO {
    boolean allowEmail = true;
    boolean allowSms = true;
    boolean allowPush = false;

    public boolean isAllowEmail() {
        return allowEmail;
    }

    public void setAllowEmail(boolean allowEmail) {
        this.allowEmail = allowEmail;
    }

    public boolean isAllowSms() {
        return allowSms;
    }

    public void setAllowSms(boolean allowSms) {
        this.allowSms = allowSms;
    }

    public boolean isAllowPush() {
        return allowPush;
    }

    public void setAllowPush(boolean allowPush) {
        this.allowPush = allowPush;
    }
}
