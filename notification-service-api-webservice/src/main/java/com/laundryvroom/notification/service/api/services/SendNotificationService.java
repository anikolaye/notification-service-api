package com.laundryvroom.notification.service.api.services;

import com.laundryvroom.notification.service.api.model.SendNotificationDTO;

/**
 * Created by alexnikolayevsky on 1/21/17.
 */
public interface SendNotificationService {

    void sendNotification (SendNotificationDTO notification);
    void sendEmail(SendNotificationDTO notification);
}
