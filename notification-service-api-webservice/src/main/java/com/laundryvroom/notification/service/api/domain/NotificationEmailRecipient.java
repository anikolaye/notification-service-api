package com.laundryvroom.notification.service.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.laundryvroom.notification.service.api.type.CopyType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * Created by alexnikolayevsky on 11/16/17.
 */
@Entity
@Table(name = "notification_email_recipient")
public class NotificationEmailRecipient {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "notification_email_fk")
    private NotificationEmail notificationEmail;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @Column(name= "copy_type")
    @Enumerated(EnumType.STRING)
    private CopyType copyType;

    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "lastmodified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public NotificationEmail getNotificationEmail() {
        return notificationEmail;
    }

    public void setNotificationEmail(NotificationEmail notificationEmail) {
        this.notificationEmail = notificationEmail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public CopyType getCopyType() {
        return copyType;
    }

    public void setCopyType(CopyType copyType) {
        this.copyType = copyType;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}
