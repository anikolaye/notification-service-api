package com.laundryvroom.notification.service.api.type;

/**
 * Created by alexnikolayevsky on 11/8/17.
 */
public enum TemplateStatus {
    DISABLED,
    ENABLED,
    TESTING
}
