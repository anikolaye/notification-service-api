package com.laundryvroom.notification.service.api.quartz.controllers;


import com.google.common.base.Preconditions;
import com.laundryvroom.notification.service.api.exceptions.InternalServerException;
import com.laundryvroom.notification.service.api.quartz.job.NotificationJobs;
import com.laundryvroom.notification.service.api.quartz.model.JobDTO;
import com.laundryvroom.notification.service.api.quartz.model.JobDetailDTO;
import com.laundryvroom.notification.service.api.quartz.model.TriggerDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.matchers.GroupMatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by alexnikolayevsky on 9/5/17.
 */
@RestController
@RequestMapping(value = "/schedule")
@Api(value = "Scheduler", description = "Scheduler")
public class SchedulingController {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private Scheduler scheduler;

    @ApiOperation(value = "Get Jobs", httpMethod = "GET")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = JobDTO.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")
    })
    @RequestMapping(value="/jobs", method = RequestMethod.GET, produces = {MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<JobDTO> getJobs(@RequestParam(defaultValue = "true", required = false) Boolean cronOnly) {
        logger.info("getJobs requested.");
        List<JobDTO> jobs = new ArrayList<JobDTO>();
        try {
            for (JobKey jobKey : scheduler.getJobKeys(GroupMatcher.anyJobGroup())) {
                JobDTO job = new JobDTO();
                job.setJobDetail(new JobDetailDTO(jobKey));

                for (Trigger trigger : scheduler.getTriggersOfJob(jobKey)) {
                    if (cronOnly && !(trigger instanceof CronTrigger)) {
                        continue;
                    }
                    job.getTriggers().add(new TriggerDTO(trigger));
                }

                jobs.add(job);
            }
        } catch (SchedulerException e) {
            logger.error("Failed to get jobs", e);
            throw new InternalServerException("Failed to get jobs.", e);
        }

        return jobs;
    }

    @ApiOperation(value = "Create Job", response = Boolean.class, httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = JobDTO.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")
    })
    @RequestMapping(value="/jobs", method = RequestMethod.POST, produces = {MediaType.APPLICATION_JSON_VALUE})
    public Boolean saveJob(@RequestParam NotificationJobs orderJob, @RequestBody JobDTO jobDto) {
        logger.info("saveJob requested.");
        saveUpdateJob(jobDto, orderJob.getJobClass());

        return true;
    }

    @ApiOperation(value = "Delete Job", response = Boolean.class, httpMethod = "DELETE")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = JobDTO.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")
    })
    @RequestMapping(value="/jobs", method = RequestMethod.DELETE)
    public Boolean deleteJob(@RequestParam(required = true) String name, @RequestParam(defaultValue = "DEFAULT", required = false) String group) {
        logger.info("deleteJob requested.");
        try {
            scheduler.deleteJob(new JobKey(name, group));
        } catch (SchedulerException e) {
            logger.error("Failed to delete job {name: '" + name + "', group: '" + group + "'}", e);
            throw new InternalServerException("Failed to delete job {name: '" + name + "', group: '" + group + "'}", e);
        }

        return true;
    }

    @ApiOperation(value = "Trigger Job", response = Boolean.class, httpMethod = "POST")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = JobDTO.class),
            @ApiResponse(code = 401, message = "Unauthorized"),
            @ApiResponse(code = 403, message = "Forbidden"),
            @ApiResponse(code = 404, message = "Not Found"),
            @ApiResponse(code = 500, message = "Failure")
    })
    @RequestMapping(value="/triggerJob", method = RequestMethod.POST)
    public Boolean triggerJob(@RequestParam(required = true) String name, @RequestParam(defaultValue = "DEFAULT", required = false) String group) {
        logger.info("triggerJob requested. name: {}, group: {}", name, group);
        try {
            scheduler.triggerJob(new JobKey(name, group));
        } catch (SchedulerException e) {
            logger.error("Failed to trigger job {name: '" + name + "', group: '" + group + "'}", e);
            throw new InternalServerException("Failed to trigger job {name: '" + name + "', group: '" + group + "'}", e);
        }

        return true;
    }

    protected void saveUpdateJob(JobDTO dto, Class jobClass) {
        Preconditions.checkNotNull(dto.getJobDetail().getName());

        if (jobClass == null) {
            try {
                jobClass = Class.forName(dto.getJobDetail().getJobClass());
            } catch (ClassNotFoundException e) {
                logger.error("Failed to find job class.", e);
                throw new InternalServerException("Failed to find job class.", e);
            }
        }

        JobDetail jobDetail = JobBuilder.newJob(jobClass)
                .withIdentity(dto.getJobDetail().getName(), dto.getJobDetail().getGroup())
                .storeDurably(true)
                .usingJobData(dto.getJobDetail().getJobDataMap())
                .build();

        Set<Trigger> triggers = new HashSet<Trigger>();
        for (TriggerDTO triggerDTO : dto.getTriggers()) {
            CronScheduleBuilder schedBuilder = null;
            try {
                schedBuilder = CronScheduleBuilder.cronSchedule(triggerDTO.getCronExpression());
            } catch (RuntimeException e) {
                logger.error("Failed to parse cron expresion.", e);
                throw new InternalServerException("Failed to parse cron expresion.", e);
            }

            Trigger trigger = TriggerBuilder.newTrigger()
                    .withIdentity(triggerDTO.getName(), triggerDTO.getGroup())
                    .withSchedule(schedBuilder)
                    .forJob(dto.getJobDetail().getName(), dto.getJobDetail().getGroup())
                    .build();

            triggers.add(trigger);
        }

        try {
            if (triggers.isEmpty()) {
                scheduler.addJob(jobDetail, true);
            } else {
                scheduler.scheduleJob(jobDetail, triggers, true);
            }
        } catch (SchedulerException e) {
            logger.error("Failed to trigger job {name: '" + dto.getJobDetail().getName() + "', group: '" + dto.getJobDetail().getGroup() + "'}", e);
            throw new InternalServerException("Failed to trigger job {name: '" + dto.getJobDetail().getName() + "', group: '" + dto.getJobDetail().getGroup() + "'}", e);
        }
    }
}

