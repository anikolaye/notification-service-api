package com.laundryvroom.notification.service.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.laundryvroom.notification.service.api.type.ChannelStatus;
import com.laundryvroom.notification.service.api.type.Status;

import java.util.Date;
import java.util.List;

/**
 * Created by alexnikolayevsky on 11/23/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class NotificationDTO {
    private Integer id;
    private Integer templateId;
    private String templateName;
    private Status status;
    private Boolean emailChannel;
    private Boolean pushChannel;
    private Boolean smsChannel;
    private Boolean immediateProcessing;
    private String rawRequest;
    private ChannelStatus emailStatus;
    private ChannelStatus smsStatus;
    private ChannelStatus pushStatus;
    private String pushResponse;
    private List<NotificationParameterDTO> parameters;
    private NotificationEmailDTO email;
    private Date created;
    private Date lastModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getTemplateId() {
        return templateId;
    }

    public void setTemplateId(Integer templateId) {
        this.templateId = templateId;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Boolean isEmailChannel() {
        return emailChannel;
    }

    public void setEmailChannel(Boolean emailChannel) {
        this.emailChannel = emailChannel;
    }

    public Boolean isPushChannel() {
        return pushChannel;
    }

    public void setPushChannel(Boolean pushChannel) {
        this.pushChannel = pushChannel;
    }

    public Boolean isSmsChannel() {
        return smsChannel;
    }

    public void setSmsChannel(Boolean smsChannel) {
        this.smsChannel = smsChannel;
    }

    public Boolean isImmediateProcessing() {
        return immediateProcessing;
    }

    public void setImmediateProcessing(Boolean immediateProcessing) {
        this.immediateProcessing = immediateProcessing;
    }

    public String getRawRequest() {
        return rawRequest;
    }

    public void setRawRequest(String rawRequest) {
        this.rawRequest = rawRequest;
    }

    public ChannelStatus getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(ChannelStatus emailStatus) {
        this.emailStatus = emailStatus;
    }

    public ChannelStatus getSmsStatus() {
        return smsStatus;
    }

    public void setSmsStatus(ChannelStatus smsStatus) {
        this.smsStatus = smsStatus;
    }

    public ChannelStatus getPushStatus() {
        return pushStatus;
    }

    public void setPushStatus(ChannelStatus pushStatus) {
        this.pushStatus = pushStatus;
    }

    public String getPushResponse() {
        return pushResponse;
    }

    public void setPushResponse(String pushResponse) {
        this.pushResponse = pushResponse;
    }

    public List<NotificationParameterDTO> getParameters() {
        return parameters;
    }

    public void setParameters(List<NotificationParameterDTO> parameters) {
        this.parameters = parameters;
    }

    public NotificationEmailDTO getEmail() {
        return email;
    }

    public void setEmail(NotificationEmailDTO email) {
        this.email = email;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}
