package com.laundryvroom.notification.service.api.services.impl;

import com.laundryvroom.notification.service.api.domain.EmailTemplate;
import com.laundryvroom.notification.service.api.domain.Template;
import com.laundryvroom.notification.service.api.domain.TemplateSearch;
import com.laundryvroom.notification.service.api.respositories.TemplateRepository;
import com.laundryvroom.notification.service.api.services.TemplateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by alexnikolayevsky on 11/21/17.
 */
@Service
public class TemplateServiceImpl implements TemplateService {
    @Autowired
    private TemplateRepository repository;

    public Template getById(Integer id) {
        return repository.findOne(id);
    }

    public Template save(Template template) {
        EmailTemplate emailTemplate = template.getEmailTemplate();
        emailTemplate.setTemplate(template);
        template.setEmailTemplate(emailTemplate);
        return repository.save(template);
    }

    public List<Template> findTemplates(TemplateSearch templateSearch) {
        return repository.findAll(templateSearch);
    }
}
