package com.laundryvroom.notification.service.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.laundryvroom.notification.service.api.type.TemplateStatus;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alexnikolayevsky on 10/20/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class EmailTemplateDTO {
    private Integer id;
    private String subject;
    private String fromEmail;
    private String fromName;
    private String replyTo;
    private String textContent;
    private String htmlContent;
    private String requiredParameters;
    private List<EmailRecipientDTO> defaultRecipients = new ArrayList<>();
    private List<EmailAttachmentDTO> defaultAttachments = new ArrayList<>();
    private Date created;
    private Date lastModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public String getRequiredParameters() {
        return requiredParameters;
    }

    public void setRequiredParameters(String requiredParameters) {
        this.requiredParameters = requiredParameters;
    }

    public List<EmailRecipientDTO> getDefaultRecipients() {
        return defaultRecipients;
    }

    public void setDefaultRecipients(List<EmailRecipientDTO> defaultRecipients) {
        this.defaultRecipients = defaultRecipients;
    }

    public List<EmailAttachmentDTO> getDefaultAttachments() {
        return defaultAttachments;
    }

    public void setDefaultAttachments(List<EmailAttachmentDTO> defaultAttachments) {
        this.defaultAttachments = defaultAttachments;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}
