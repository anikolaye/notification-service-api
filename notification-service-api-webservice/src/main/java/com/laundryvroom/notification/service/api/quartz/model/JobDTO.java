package com.laundryvroom.notification.service.api.quartz.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by alexnikolayevsky on 9/5/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class JobDTO {
    private JobDetailDTO jobDetail;
    private Set<TriggerDTO> triggers = new HashSet<TriggerDTO>();

    public JobDetailDTO getJobDetail() {
        return jobDetail;
    }

    public void setJobDetail(JobDetailDTO jobDetail) {
        this.jobDetail = jobDetail;
    }

    public Set<TriggerDTO> getTriggers() {
        return triggers;
    }

    public void setTriggers(Set<TriggerDTO> triggers) {
        this.triggers = triggers;
    }
}
