package com.laundryvroom.notification.service.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by drobinson on 2/5/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RecipientDTO {
    private EmailRecipientDTO emailRecipient;
    private SmsDTO smsRecipient;
    private PushDTO pushRecipient;

    public EmailRecipientDTO getEmailRecipient() {
        return emailRecipient;
    }

    public void setEmailRecipient(EmailRecipientDTO emailRecipient) {
        this.emailRecipient = emailRecipient;
    }

    public SmsDTO getSmsRecipient() {
        return smsRecipient;
    }

    public void setSmsRecipient(SmsDTO smsRecipient) {
        this.smsRecipient = smsRecipient;
    }

    public PushDTO getPushRecipient() {
        return pushRecipient;
    }

    public void setPushRecipient(PushDTO pushRecipient) {
        this.pushRecipient = pushRecipient;
    }
}
