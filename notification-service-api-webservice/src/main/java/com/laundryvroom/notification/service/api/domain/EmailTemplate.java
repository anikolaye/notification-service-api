package com.laundryvroom.notification.service.api.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.laundryvroom.notification.service.api.type.TemplateStatus;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by drobinson on 2/5/2017.
 */
@Entity
@Table(name = "email_template")
public class EmailTemplate {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "template_fk")
    private Template template;

    @Column(name = "subject")
    private String subject;

    @Column(name = "from_email")
    private String fromEmail;

    @Column(name = "from_name")
    private String fromName;

    @Column(name = "reply_to")
    private String replyTo;

    @Column(name = "text_content")
    private String textContent;

    @Column(name = "html_content")
    private String htmlContent;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = EmailRecipient.class, mappedBy = "emailTemplate", cascade = CascadeType.ALL)
    private List<EmailRecipient> defaultRecipients = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = EmailAttachment.class, mappedBy = "emailTemplate", cascade = CascadeType.ALL)
    private List<EmailAttachment> defaultAttachments = new ArrayList<>();

    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "lastmodified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Template getTemplate() {
        return template;
    }

    public void setTemplate(Template template) {
        this.template = template;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getFromEmail() {
        return fromEmail;
    }

    public void setFromEmail(String fromEmail) {
        this.fromEmail = fromEmail;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getReplyTo() {
        return replyTo;
    }

    public void setReplyTo(String replyTo) {
        this.replyTo = replyTo;
    }

    public String getTextContent() {
        return textContent;
    }

    public void setTextContent(String textContent) {
        this.textContent = textContent;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public List<EmailRecipient> getDefaultRecipients() {
        return defaultRecipients;
    }

    public void setDefaultRecipients(List<EmailRecipient> defaultRecipients) {
        this.defaultRecipients = defaultRecipients;
    }

    public List<EmailAttachment> getDefaultAttachments() {
        return defaultAttachments;
    }

    public void setDefaultAttachments(List<EmailAttachment> defaultAttachments) {
        this.defaultAttachments = defaultAttachments;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}
