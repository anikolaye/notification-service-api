package com.laundryvroom.notification.service.api.quartz.job;

/**
 * Created by alexnikolayevsky on 9/5/17.
 */
public enum NotificationJobs {
    NOTIFY_JOB(NotifyJob.class);

    private Class jobClass;

    NotificationJobs(Class jobClass) {
        this.jobClass = jobClass;
    }

    public Class getJobClass() {
        return jobClass;
    }

    public void setJobClass(Class jobClass) {
        this.jobClass = jobClass;
    }
}
