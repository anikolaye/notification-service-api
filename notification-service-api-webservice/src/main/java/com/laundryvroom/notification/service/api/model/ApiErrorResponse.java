package com.laundryvroom.notification.service.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.common.base.Objects;
import com.google.common.collect.Lists;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by drobinson on 2/5/2017.
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ApiErrorResponse {
    @XmlElement
    private Integer httpStatusCode;

    @XmlElement
    private List<String> errors = Lists.newArrayList();

    @XmlElement
    private String requestURI;

    public ApiErrorResponse() {}

    public ApiErrorResponse(List<String> errors) {
        this.errors = errors;
    }

    public ApiErrorResponse(String error) {
        this(Collections.singletonList(error));
    }

    public ApiErrorResponse(String... errors) {
        this(Arrays.asList(errors));
    }

    public Integer getHttpStatusCode() {
        return httpStatusCode;
    }

    public void setHttpStatusCode(Integer httpStatusCode) {
        this.httpStatusCode = httpStatusCode;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    /**
     *
     * @param errorMessage
     * @return
     */
    public boolean addErrorMessage(String errorMessage){
        return this.errors.add(errorMessage);
    }

    public String getRequestURI() {
        return requestURI;
    }

    public void setRequestURI(String requestURI) {
        this.requestURI = requestURI;
    }

    @Override
    public String toString() {
        return Objects.toStringHelper(this)
                .add("httpStatusCode", httpStatusCode)
                .add("errors", errors)
                .add("requestURI", requestURI)
                .toString();
    }
}
