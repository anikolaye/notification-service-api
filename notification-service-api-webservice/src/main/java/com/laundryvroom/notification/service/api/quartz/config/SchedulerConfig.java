package com.laundryvroom.notification.service.api.quartz.config;

import com.laundryvroom.notification.service.api.quartz.spring.AutowiringSpringBeanJobFactory;
import com.laundryvroom.notification.service.api.util.DotConfigurationProperties;
import org.quartz.spi.JobFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import javax.sql.DataSource;

/**
 * Created by alexnikolayevsky on 9/5/17.
 */
@Configuration
public class SchedulerConfig {
    @Bean
    @ConfigurationProperties("quartz.datasource")
    public DataSource quartzDataSource() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    public JobFactory jobFactory(ApplicationContext applicationContext) {
        AutowiringSpringBeanJobFactory jobFactory = new AutowiringSpringBeanJobFactory();
        jobFactory.setApplicationContext(applicationContext);
        return jobFactory;
    }

    @Bean
    public SchedulerFactoryBean schedulerFactoryBean(@Qualifier("quartzDataSource") DataSource dataSource, @Qualifier("jobFactory") JobFactory jobFactory, @Qualifier("quartzProperties") DotConfigurationProperties properties) throws Exception {
        SchedulerFactoryBean factory = new SchedulerFactoryBean();
        // this allows to update triggers in DB when updating settings in config file:
        factory.setOverwriteExistingJobs(true);
        factory.setDataSource(dataSource);
        factory.setJobFactory(jobFactory);
        factory.setQuartzProperties(properties.getFlatProperties());

        return factory;
    }

    @Bean
    @ConfigurationProperties("scheduler")
    public DotConfigurationProperties quartzProperties() {
        return new DotConfigurationProperties();
    }
}