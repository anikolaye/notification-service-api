package com.laundryvroom.notification.service.api.domain;

import com.laundryvroom.notification.service.api.type.TemplateStatus;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by alexnikolayevsky on 11/20/17.
 */
@Entity
@Table(name = "template")
public class Template {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "template_name")
    private String templateName;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private TemplateStatus status;

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToMany(targetEntity = TemplateParameter.class, mappedBy = "template", cascade = CascadeType.ALL)
    private List<TemplateParameter> defaultParameters = new ArrayList<>();

    @LazyCollection(LazyCollectionOption.FALSE)
    @OneToOne(targetEntity = EmailTemplate.class, mappedBy = "template", cascade = CascadeType.ALL)
    private EmailTemplate emailTemplate;

    @Column(name = "created", insertable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Date created;

    @Column(name = "lastmodified")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastModified;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public TemplateStatus getStatus() {
        return status;
    }

    public void setStatus(TemplateStatus status) {
        this.status = status;
    }

    public List<TemplateParameter> getDefaultParameters() {
        return defaultParameters;
    }

    public void setDefaultParameters(List<TemplateParameter> defaultParameters) {
        this.defaultParameters = defaultParameters;
    }

    public EmailTemplate getEmailTemplate() {
        return emailTemplate;
    }

    public void setEmailTemplate(EmailTemplate emailTemplate) {
        this.emailTemplate = emailTemplate;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }
}
