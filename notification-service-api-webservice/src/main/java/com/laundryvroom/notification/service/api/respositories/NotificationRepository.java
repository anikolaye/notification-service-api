package com.laundryvroom.notification.service.api.respositories;

import com.laundryvroom.notification.service.api.domain.Notification;
import com.laundryvroom.notification.service.api.type.Status;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by alexnikolayevsky on 5/24/16.
 */
public interface NotificationRepository extends CrudRepository<Notification, Integer> {
    List<Notification> findByStatus(Status status);
}
