Email Templates
```
{
  "bcc": "",
  "created": "2017-11-02T02:53:57.618Z",
  "fromEmail": "noreply@laundryvroom.com",
  "fromName": "LaundryVroom",
  "id": 0,
  "lastModified": "2017-11-02T02:53:57.618Z",
  "replyTo": "",
  "requiredParameters": "",
  "subject": "Your Laundry Is Being Washed!",
  "templateName": "OrderSuccess"

}
```
```
{
  "channels": [
    "EMAIL"
  ],
  "content": {
    "emailHtml": "<html>\r\n                      <head>\r\n                          <title>Your Laundry Is Being Washed!<\/title>\r\n                      <\/head>\r\n                      <body>\r\n                          <table>\r\n                              <tr>\r\n                                  <td>\r\n                                      LaundryVroom LLC\r\n                                  <\/td>\r\n                              <\/tr>\r\n                              <tr>\r\n                                  <td>\r\n                                      Woo Hoo! Your laundry is being washed.  Sit back and relax and we'll let you know when it's ready!\r\n                                  <\/td>\r\n                              <\/tr>\r\n                              <tr>\r\n                                <td>\r\n                                    LaundryVroom LLC All Rights Reserved\r\n                                <\/td>\r\n                              <\/tr>\r\n                          <\/table>\r\n                      <\/body>\r\n                  <\/html>",
    "emailText": "Woo Hoo!  Your laundry is being washed.  Sit back and relax and we'll let you know when it's ready!",
    "push": "",
    "sms": ""
  },
  "recipient": {
    "email": {
      "bccRecipients": "",
      "ccRecipients": "",
      "fromEmail": "noreply@mail.laundryvroom.com",
      "replyTo": "",
      "subject": "Your Laundry Is Being Washed!",
      "tag": "",
      "toRecipients": "drobinson75@yahoo.com"
    },
    "push": {},
    "sms": {}
  },
  "userPreferences": {
    "allowEmail": true,
    "allowPush": true,
    "allowSms": true
  },
  "templateName":"OrderSuccess"
}
```
#Templates
- Customer
    - Order Confirmation (email)
    - Driver on the way to pickup/Arrived (push/sms)
    - Laundry Ready (push/email)
    - Laundry Being Driven/Push Notificaion (Push)?
    - Order Complete
    - Marketing Place new Orders (Push/Email)

- Laundromat
    - New Order Being Sent To You (Something new in the system)
    - Invoices (Monthly)

- Driver
    - Push notifications of orders in their area
    - Reminders to sign on
    - Monthly Invoice